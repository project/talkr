This is an easy to use module that adds a link to your site's Talkr.com
content via an embedded link. You will need to visit talkr.com, 
register
for an account and then register your site. Talkr.com will then scan 
your
site periodically and convert your text content to an audio file, 
perfect
for podcasting. 

This script is licensed under the GPL.